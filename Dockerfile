FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP jitsi-data.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache git
RUN apk add --no-cache gcc musl-dev linux-headers
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["flask", "run"]
